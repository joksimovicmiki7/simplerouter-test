<?php
namespace Controllers;

use Models\News;
use Database\Mysql;

class NewsController
{
    public function __construct()
    {
        Mysql::getConnection();
    }

    public static function store($data)
    {
        return News::create($data);
    }

    public static function getCategorized($id)
    {
        return News::with('category')->where('category_id', '=', $id)->get()->toArray();
    }

    public static function getAll()
    {
        return News::get()->toArray();
    }
}
