<?php
namespace Controllers;

use Models\Category;
use Database\Mysql;

class CategoryController
{
    public function __construct()
    {
        Mysql::getConnection();
    }

    public static function store($name)
    {
        return Category::create(['name' => $name]);
    }

    public static function getNews($id)
    {
        return Category::with('news')->where('id', '=', $id)->get()->toArray();
    }

    public static function getName($id)
    {
        return Category::where('id', '=', $id)->pluck('name');
    }

    public static function getCategories()
    {
        return Category::get()->toArray();
    }
}
