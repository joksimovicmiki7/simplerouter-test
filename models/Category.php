<?php
namespace Models;

use illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $fillable = ['title', 'image', 'description'];

   
    public function news()
    {
        return $this->hasMany('\Models\News');
    }
}
