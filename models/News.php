<?php
namespace Models;

use illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';
    protected $fillable = ['title', 'image', 'description', 'category_id'];

    public function category()
    {
        return $this->belongsTo('\Models\Category');
    }

    public static function getCategories()
    {
        return Category::get()->toArray();
    }
}
