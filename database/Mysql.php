<?php
 
namespace Database;

use Illuminate\Database\Capsule\Manager as Capsule;
 
class Mysql
{
    public function __construct()
    {
        $capsule = new Capsule;
        $capsule->addConnection([
        "driver" => DRIVER,
        "host" => HOST,
        "database" => NAME,
        "username" => USER,
        "password" => PASS,
        "charset" => "utf8",
        "collation" => "utf8_unicode_ci",
        "prefix" => "",
        ]);

        $capsule->bootEloquent();
    }

    public static function getConnection()
    {
        return new static;
    }
}
