<?php
require_once 'helper.php';

use Pecee\SimpleRouter\SimpleRouter;
use Controllers\NewsController;

SimpleRouter::setDefaultNamespace('/cms');
SimpleRouter::get('/cms', function () {
    require_once "index.php";
});

SimpleRouter::get('/get_news', [NewsController::class, 'getAll']);

SimpleRouter::get('/login', function () {
    require_once "login.php";
});

SimpleRouter::get('/cms/logout', function () {
    $test = 'eeee';
    echo $test;
    // require_once "logout.php";
});
SimpleRouter::get('/cms/register', function () {
   redirect('localhost/cms/registration.php');
});


SimpleRouter::start();
